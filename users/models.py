# from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django.contrib.auth.models import AbstractUser
from users.manager import CustomUserManager
# Create your models here.
# inherit the user properties from abstract user model
class User(AbstractUser):
    name = models.CharField(max_length=200)
    email = models.CharField(max_length=255, unique=True)
    password = models.CharField(max_length=200)
    username = None

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    object = CustomUserManager()
class Ebooks(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    serial_no = models.IntegerField(null=True,unique=True)