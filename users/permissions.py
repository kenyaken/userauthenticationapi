'''A custom permission settings authenticates only the author to have read and write authorization'''
from rest_framework import permissions

class IsAuthorOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        # checks whether the request made 
        if request.method in permissions.SAFE_METHODS:
            return True
        # read only permissions allowed for a request
        return obj.author == request.user