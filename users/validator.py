from rest_framework.exceptions import AuthenticationFailed
def validate_password(password):
    SpecialSymbol =['$', '@', '#', '%'] 
    if len(password) < 6: 
        raise AuthenticationFailed('length should be at least 6') 
    if len(password) > 20: 
        raise AuthenticationFailed('length should be not be greater than 8')    
    if not any(char.isdigit() for char in password): 
        raise AuthenticationFailed('Password should have at least one numeral') 
            
    if not any(char.isupper() for char in password): 
        raise AuthenticationFailed('Password should have at least one uppercase letter') 
        
    if not any(char.islower() for char in password): 
        raise AuthenticationFailed('Password should have at least one lowercase letter')    
        
    if not any(char in SpecialSymbol for char in password): 
        raise AuthenticationFailed('Password should have at least one of the symbols $@#') 