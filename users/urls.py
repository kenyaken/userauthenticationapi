from django.contrib import admin
from django.urls import path,include
from .views import EbooksPostView, LoginView, LogoutView, RegisterView, UserView,EbooksPost
urlpatterns = [
    path('register',RegisterView.as_view()),
    path('login',LoginView.as_view()),
    path('user',UserView.as_view()),
    path('logout',LogoutView.as_view()),
    path('ebooks_post',EbooksPostView.as_view()),
    path('ebooks',EbooksPost.as_view({"get":"list","post":"create"})),
    # the path below is for retrieving individual book posted with unique id
    path('ebooks/<pk>',EbooksPost.as_view({"get":"retrieve","put":"update"})), 

]