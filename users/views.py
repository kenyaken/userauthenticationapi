from django.shortcuts import render
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from .serializers import UserSerializer,EbooksSerializer
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed
from .models import Ebooks, User
from .validator import validate_password
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth import settings
from django.contrib.auth.models import update_last_login
# import jwt for authentication
import jwt
import datetime
from .permissions import IsAuthorOrReadOnly
# Create your views here.
class RegisterView(APIView):
    # apiview has a get and a post function
    def post(self, request):
        # SpecialSymbol =['$', '@', '#', '%'] 
        serializer = UserSerializer(data=request.data)
        # get the password 
        serializer.is_valid(raise_exception=True)
        password = request.data['password']
        # get the validated password
        validate_password(password) 
        # validate the serializer
        # serializer.is_valid(raise_exception=True)
        # save the serializer
        serializer.save()
        return Response(serializer.data)

# create a login function
class LoginView(APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        email = request.data['email']
        password = request.data['password']
        # get the user from the database using the unique email.
        user = User.objects.filter(email=email).first()

        if user is None:
            raise AuthenticationFailed("User not found! Try creating an account with us")
        # since the password is hashed, use the inbuilt check password function to get the password
        if not user.check_password(password):
            raise AuthenticationFailed("Incorrect password")
        
        # to use jwt we need to initialize the payload
        # payload = {
        #     'id':user.id,
        #     'exp': datetime.datetime.utcnow()+ datetime.timedelta(minutes=60),
        #     'iat': datetime.datetime.utcnow()
        # }

        refresh = RefreshToken.for_user(user)
        refresh_token = str(refresh)
        access_token = str(refresh.access_token)
        validation = {}
        validation["token"] = {
                    "access": access_token,
                    "refresh": refresh_token,
                    "lifetime": refresh.lifetime,
                    "expiry_time": refresh.lifetime
                }


        update_last_login(None, user)
        
        # token = jwt.encode(payload,settings.SECRET_KEY, algorithm='HS256').decode('utf-8')
        
        # response = Response()
        # response.set_cookie(key='jwt',value=token,httponly=True)
       
        return Response(validation,status=200)

class UserView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        # token = request.COOKIES.get('jwt')
        # # decode the cookie to get the data
        # if not token:
        #     raise AuthenticationFailed("Unauthenticated")
        
        # try:
        #     payload = jwt.decode(token, 'secret',algorithms=['HS256'])
        # except jwt.ExpiredSignatureError:
        #     raise AuthenticationFailed("Unauthenticated")
        # extract 
        user = User.objects.all()
        serializer = UserSerializer(user, many=True)
        return Response(serializer.data)
    # def retrieve(self, request):
    #     user = User.objects.get

class LogoutView(APIView):
    def post(self, request):
        response = Response()
        response.delete_cookie('jwt')
        response.data =  {
            'message':'success'
        }
        return response

# approach 1 ebooks model
class EbooksPost(ModelViewSet):
    serializer_class = EbooksSerializer
    queryset = Ebooks.objects.all()
# approach 2 ebooks model
class EbooksPostView(APIView):
    
    # posting new data
    def post(self, request):
        permission_classes = (IsAuthorOrReadOnly,)
        #  deserialize the data
        serializer = EbooksSerializer(data = request.data)
        serial_no = request.data['serial_no']
        # check is serial number is equal to six
        if len(serial_no) != 6:
            raise AuthenticationFailed("serial number must have six numbers")
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    # method to list the posted data
    def get(self, request):
        # request = request.data['id']
        data = Ebooks.objects.all()
        serializer = EbooksSerializer(data,many=True)
        return Response(serializer.data)


