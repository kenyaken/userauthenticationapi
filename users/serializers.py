from rest_framework import serializers
from .models import User,Ebooks
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        # specify the fields you want to use
        fields = ['id','name','email','password']
        # option for each property i.e password for write only purposes
        extra_kwargs = {
            'password':{'write_only':True}
        }
    # hash the password by overriding the default created data
    def create(self,validated_data):
        # extract the password
        password = validated_data.pop('password',None) #exclude password since its been popped
        #  create the user
        instance = self.Meta.model(**validated_data)

        if password is not None:
            # the password is now hashed
            instance.set_password(password)
        instance.save()
        return instance

class EbooksSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ebooks

        fields = ['id','title','author','serial_no']

        